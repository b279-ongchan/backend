// console.log("Hello World")

// arithmetic operators

let x=1397, y=7831;
let sum=x+y;
console.log("Result of addition operator: "+sum);

let difference=x-y;
console.log("Result of subtraction operator: "+difference);

let product=x*y;
console.log("Result of multiplication operator: "+product);

let quotient=x/y;
console.log("Result of division operator: "+quotient);

// modulus
let remainder=y%x;
console.log("Result of modulo operator: "+ remainder);

// assignment operator
// basic assignment operator (=) - adds the value of the right opperand to a variable

let assignmentNumber=8;
assignmentNumber=assignmentNumber+2;
console.log("Result of addition assignment operator: "+ assignmentNumber);

assignmentNumber+=2;
console.log("Result of addition assignment operator: "+ assignmentNumber);

// (-=, *=, /=)
assignmentNumber-=2;
console.log("Result of subtraction assignment operator: "+ assignmentNumber);

assignmentNumber*=2;
console.log("Result of multiplication assignment operator: "+ assignmentNumber);

assignmentNumber/=2;
console.log("Result of division assignment operator: "+ assignmentNumber);

// multiple operators and parenthesis

let mdas= 1+2-3*4/5;
console.log("result from mdas: "+ mdas);

/*
            - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
        */
let pemdas= 1+(2-3)*(4/5);
console.log("result from pemdas: "+ pemdas);

/*
            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2
        */

// increment and decrement
// operators that add or subtract values by 1 and re-assign the value of the variable.
// incrementation ++
// decrementation --

let z=1;
// the value of z is added by a value of one before returning the value and storing it in the variable increment

let increment=++z;
console.log("Result of pre-incrementation: "+ increment);
console.log("result of pre-incrementation: "+z);
 increment=z++;
console.log("Result of post-incrementation: "+ increment);
console.log("result of post-incrementation: "+z);

decrement=--z;
console.log("Result of pre-decrementation: "+ decrement);
console.log("result of pre-decrementation: "+z);

decrement=z--;
console.log("Result of post-decrementation: "+ decrement);
console.log("result of post-decrementation: "+z);

// type coercion
let numA="10";
let numB=12;

let coercion=numA+numB;
console.log(coercion)
console.log(typeof coercion);

/* 
            - Adding/concatenating a string and a number will result is a string
            - This can be proven in the console by looking at the color of the text displayed
            - Black text means that the output returned is a string data type
        */
let numC=16, numD=14;

let nonCoercion=numC+numD;
console.log(nonCoercion)
console.log(typeof nonCoercion);

let numE=true+1;
console.log(numE);

let numF=false+1;
console.log(numF);

// comparison opeartor

let juan="juan";

// equality operator
// returns boolean value

console.log(1==1);
console.log(1==2);
console.log(1=="1");
console.log(0==false);
console.log("juan"=='juan')
console.log("juan"==juan)

// inequality operator (!=) ! -> not

console.log(1!=1);
console.log(1!=2);
console.log(1!="1");
console.log(0!=false);
console.log("juan"!='juan')
console.log("juan"!=juan)

// strict equality operator (===) = same content and data type
console.log(1==="1");
console.log(0===false);

// Strict Inequality Operator (!==)
console.log(1 !== "1");
console.log(0 !== false);

// Relational Operators
// Some comparison operators chech whether one value is greater than or less than to the other value.

let a = 50, b = 65;

// GT or Greater Than (>)
console.log(a + "," + b);

let greaterThan = a > b;
console.log(greaterThan);

let lessThan = a < b;
console.log(lessThan);

let GTorEqual = a >= b;
console.log(GTorEqual);

let LTorEqual = a <= b;
console.log(LTorEqual);

let numStr = "30";
console.log(a > numStr); //true -> forced coercion to change sting to number
console.log(b <= numStr);

let str = "twenty";
console.log(b >= str);
// false
// Since the string is not numeric, The string was converted to a number and it resulted NaN = Not a Number.

// Logical Operators

let isLegalAge = true;
let isRegistered = false;

//Logial AND "&&" - Double Ampersand
// Returns True if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR "||" - Double Pipe
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Logical NOT "!" - Exclamation Point
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result from Logical NOT Operator: " + someRequirementsNotMet);








