// SERVER CREATION AND DB CONNECTION
const express = require("express");
const mongoose=require("mongoose");
// Allows us to control the App's Cross Origin Resource Sharing
const cors = require("cors");
const userRoutes=require("./routes/user.js");
const courseRoutes=require("./routes/course.js")


const app = express();

// mongo db connection using srv link
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.sk2eror.mongodb.net/Course_Booking_System?retryWrites=true&w=majority", 
{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

// optional - validation of DB connection
mongoose.connection.once("open", ()=> console.log("Now connected to MongoDB Atlas."));


// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all the user routes defined in the "user.js" route file.
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);






// port listening
if (require.main === module){
	app.listen(process.env.PORT||4000, () => console.log(`API is now online on port ${process.env.PORT||4000}`));
}
module.exports=app;