const express=require("express");
const router = express.Router();
const userController=require("../controllers/user.js")
const auth=require("../auth.js")

router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController =>res.send(resultFromController));
});

// route for registering a user
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

// route for user login
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
});

// route for details
router.post("/details", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile(userData.id).then(resultFromController=>res.send(resultFromController))
});

// Enroll user to a course
router.post("/enroll", auth.verify, (req,res)=>{
	let data={
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	userController.enroll(data).then(resultFromController=>res.send(resultFromController))
})


// allows us to export the "router" object that will be accessed in out index.js file
module.exports=router;