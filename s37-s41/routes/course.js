const express=require("express");
const router = express.Router();
const courseController=require("../controllers/courseController.js");
const auth=require("../auth.js");

// route for creating a course
router.post("/", auth.verify, (req,res)=>{
	const data={
		course:req.body,
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController));
});

// Get all active courses
router.get("/", (req,res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Get all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

//Retrieve Specific Course
router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

//Update Specific Course
router.put("/:courseId", auth.verify,(req,res)=>{
	let isAdmin=auth.decode(req.headers.authorization).isAdmin;

	courseController.updateCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Archive Specific Course
router.patch("/:courseId/archive", auth.verify,(req,res)=>{
	let isAdmin=auth.decode(req.headers.authorization).isAdmin;

	courseController.archiveCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

// allows us to export the "router" object that will be accessed in our index.js file
module.exports=router;