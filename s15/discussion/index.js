console.log("Hello World");

// [SECTION] Syntax, Statemets, Comments

// To add comment, we are using "//" or CMD + / -> single line comment

/* cmd+option+/ for block comment */

// Syntax and Statements
// JS Statements usually ends with a semi-colon (;)
// Statements -> in programming are instruction that we tell our computer to perform
// semi-colons are not required in JS, but we will use it to help a string locate where a statement ends

// [SECTION] Variables
// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
// This makes it easier for us associate information stored in our devices to actual "names" about information

// Variable Declaration
// declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

// syntax -> var/let/const variableName;
// variable names should be unique and should not be declared multiple times

let myVariable;
console.log(myVariable);

// We cannot use an undeclared variable. It should be dec
let hello;
console.log(hello);

// CAMEL CASING -> thisIsCamelCasing
// SNAKE CASING -> this_is_snake_casing
// KEBAB CASING -> this-is-kebab-casing



/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/

// declaring and initializing variable
// let/const variableName=value;

let productName= "desktop computer";
console.log (productName);

let productPrice = 28999;
console.log (productPrice);


// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended

const interest = 3.539;
const hoursInADay=24;

// Re-assign a value
// Changing its initial or previous value into another value
// Syntax -> variableName= newValue;
productName="Laptop";
console.log(productName);

/*// interest=5.2;
console.log(interest);*/


// let vs const
// let -> variable values can be changed
// const -> variable values cannot be update

// declare variable first
let supplier;
supplier="John Smith Tradings";
console.log(supplier);

supplier="Zuitt Store"
console.log(supplier);

// error must declare immediately
/*const pi;
pi=3.1416;
console.log(pi);*/

// HOISTING -> default behaviour of moving declaration on the top

a=5;
console.log(a);
var a

// multiple varaible declarations
// usually declared in one line, quicker

/*let productCode="DCO17";
let productBrand="Dell";*/

let productCode="DCO17", productBrand="Dell";
console.log(productCode, productBrand);

// [Section] Data Types
// Strings - series of characters that creates a word, phrase a sentence or anything related to creating a text
// Strings in JS can be written using sing (' ') or double (" ") quote
let country ="Philippines";
let province ='Metro Manila';

// concatenating strings - multiple strings can be combined to create a single string using + symbol

// sample output -> Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the "+ country;
console.log(greeting);

// Escape Characters(\)
// "\n" refers to creating a new line

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

//Using double quotes along with single quotes
let message="John's employees went home early";
console.log(message);

message='John\'s employees went home';
console.log(message);

//numbers

//integers/ whole numbers

let headcount = 26;
console.log(headcount);

let grade=98.7;
console.log(grade);

// exponential notation
let planetDistance=2e10
console.log(planetDistance);

// combining of text and int
console.log("John's grade last quarter is " + grade);

// Boolean - normally used to store values relating to the state of certain thing
let isMarried=false;
let isGoodConduct=true;

console.log("isMarried? "+ isMarried);
console.log("isGoodConduct "+ isGoodConduct);

//Arrays
// Arrays are a special kind of data type thats used to store multiple values.
// Arrays can store different data types but is normally used to store similar data types.
// Syntax let/const arrayName =[]

let grades=[98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types ( not recommended )
let details = ["john", "smith", 32, true];
console.log(details);

// objects
// objects are another special kinds of data type
// it has property and values -> key pairs
/*

let/const objectName={
	propertyA:value;
	propertyB:value;
}

*/

let person={
	fullName:"Juan Dela Cruz",
	age:35,
	isMarried:false,
	contact:["+6312345", "+6398765"],
	address:{
		houseNumber: "345",
		city:"Manila"
	}
}

console.log(person);

// useful for creating abstract objects
let myGrades={
	firstGrading:98.7,
	secondGrading:92.1,
	thirdGrading:98.2,
	fourthGrading:94.6
}

console.log(myGrades);

//"typeof" - used to determine the type of data
console.log(typeof isMarried);
console.log(typeof person);
console.log(typeof grades);

// constant object and arrays

const anime=["one piece", "one punchman", "attack on titan"];
anime[0]="Kimetsu No Yaiba";
console.log(anime);

//null
//It is used to intentionally express the absence of a value
// null=0=" "
// this means the variable does not hold any value;

let spouse=null;
let myNumber=0;
let myString= " ";

// undefined -represents the state of variable that has been declared without initial value
 





