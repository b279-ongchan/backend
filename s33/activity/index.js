// retrieve all to do list items + array

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response=>response.json())
.then(title=>{
	let titleMap=title.map((dataToMap)=> dataToMap.title)
	console.log(titleMap);
});


// retrieve single to do list 
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res=>res.json())
.then(result=>console.log("The item "+result.title+" on the list has a status of "+ result.completed));

// create to do list
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Items",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// update to do list
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		title: "Updated To Do List Items",
		description: "To update the my to do list with a different data structure",
		status: "pending",
		dateCompleted: "Pending",
		userId: 1,
		
		
	})
})
.then(res => res.json())
.then(json => console.log(json));

// PATCH

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/2021",
		
	})
})
.then(res => res.json())
.then(json => console.log(json));


// delete post
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE" });